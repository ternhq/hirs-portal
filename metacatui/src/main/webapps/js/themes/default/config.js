var theme = theme || "default";
var themeTitle = "Live Learning Library, Faculty of Science - The University of Queensland, Australia";
var themeMap = 
{
	'*': {
		// example overrides are provided here
		//'views/AboutView' : 'themes/' + theme + '/views/AboutView.js',
		'templates/faq.html' : 'themes/' + theme + '/templates/faq.html',
		'routers/router' : 'js/themes/' + theme + '/routers/router.js'
		}
};

var presetLicenses = [ {
    code: 'CC BY 4.0',
    name: 'Creative Commons - Attribution 4.0 International',
    url: 'http://creativecommons.org/licenses/by/4.0/',
    text: 'This work is licensed under Creative Commons - Attribution 4.0 International. The licence allows others copy, distribute, display, and perform the work and derivative works based upon it provided that they credit the original source and any other nominated parties.'
},{ 
    code: 'CC BY-NC 4.0',
    name: 'Creative Commons - Attribution‐NonCommercial 4.0 International',
    url: 'http://creativecommons.org/licenses/by-nc/4.0',
    text: 'This work is licensed under Creative Commons - Attribution-NonCommercial 4.0 International. The licence allows others copy, distribute, display, and perform the work and derivative works based upon it but only if: (1) it is for non-commercial purposes and (2) they credit the original creator/s and any other nominated parties.'
},{ 
    code: 'CC BY-SA 4.0',
    name: 'Creative Commons - Attribution-ShareAlike 4.0 International',
    url: 'http://creativecommons.org/licenses/by-sa/4.0/',
    text: 'This work is licensed under Creative Commons - Attribution-ShareAlike 4.0 International. The licence allows others to distribute derivative works only under a license identical to the license that governs the work.'
}];








