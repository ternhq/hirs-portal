/*global define */
define(['jquery', 'underscore', 'backbone', 'text!templates/mainContent.html'], 				
	function($, _, Backbone, MainContentTemplate) {
	'use strict';
	
	// Build the main content view of the application
	var MainContentView = Backbone.View.extend({

		el: '#mainContent',
		
		tagName: 'section',
		
		template: _.template(MainContentTemplate),
		
		initialize: function () {
		},
		
		events: {
			'click #search_btn_main': 'triggerSearch',
			'keypress #search_txt_main': 'triggerOnEnter'
		},
				
		render: function () {			
			this.$el.html(this.template());

			//set maincontent height,ys
		//	this.$el.height("600px");
		//	this.$el.css('margin-top',100+'px');
	       //		this.$el.css("margin-bottom","100px");			
			return this;
		},	
		
		triggerSearch: function() {
			// alert the model that a search should be performed
			var searchTerm = $("#search_txt_main").val();
			
			//Clear the search model to start a fresh search
			appSearchModel.clear().set(appSearchModel.defaults);
			
			//Create a new array with the new search term
			var newSearch = [searchTerm];
			
			//Set up the search model for this new term
			appSearchModel.set('all', newSearch);
			
			// make sure the browser knows where we are going
			uiRouter.navigate("data", {trigger: true});
			
			// ...but don't want to follow links
			return false;
			
		},
		
		triggerOnEnter: function(e) {
			if (e.keyCode != 13) return;
			this.triggerSearch();
		}
			
				
	});
	return MainContentView;		
});
