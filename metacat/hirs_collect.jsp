<%@ page language="java" trimDirectiveWhitespaces="true" 
         import="java.sql.*" %>
<% 
String organisation = request.getParameter("organisation");
String user_type = request.getParameter("user_type");
String reason = request.getParameter("reason");
String docid = request.getParameter("docid");
String packageid = request.getParameter("packageid");
int userid = 0;

for (String param : new String[]{organisation, user_type, reason, docid, packageid}) {
  if (param == null || param.isEmpty()) {
    response.sendError(400, "Incomplete parameters");
    return;
  }
}

organisation = organisation.toLowerCase();
user_type = user_type.toLowerCase();
reason = reason.toLowerCase();

try {
  String driver = "org.postgresql.Driver";
  String url = "jdbc:postgresql://localhost:5432/metacat";
  String username = "metacat";
  String password = "metacat";
  String userQuery = "SELECT id FROM hirs_users WHERE organisation = ? AND user_type = ? AND reason = ?";
  String userInsert = "INSERT INTO hirs_users (organisation, user_type, reason) VALUES (?, ?, ?) RETURNING id";
  String downloadInsert = "INSERT INTO hirs_download (userid, docid, packageid) VALUES (?, ?, ?)";
  Connection conn;
  PreparedStatement ps;
  ResultSet rs;
  
  Class.forName(driver).newInstance();
  conn = DriverManager.getConnection(url, username, password);
  ps = conn.prepareStatement(userQuery);
  ps.setString(1, organisation);
  ps.setString(2, user_type);
  ps.setString(3, reason);
  rs = ps.executeQuery();
  if (rs.next()) {
    userid = rs.getInt("id");
  } else {
    ps = conn.prepareStatement(userInsert);
    ps.setString(1, organisation);
    ps.setString(2, user_type);
    ps.setString(3, reason);
    rs = ps.executeQuery();
    if (rs.next()) userid = rs.getInt("id");
  }
  if (userid == 0) {
    response.sendError(500, "cannot get user id");
    return;
  }

  ps = conn.prepareStatement(downloadInsert);
  ps.setInt(1, userid);
  ps.setString(2, docid);
  ps.setString(3, packageid);
  int rows = ps.executeUpdate();
  if (rows == 0) {
    response.sendError(500, "cannot insert record");
    return;
  }
  
} catch(Exception e) {
  response.sendError(500, e.toString());
}
%>
OK

