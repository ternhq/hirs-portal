<%@ page language="java" trimDirectiveWhitespaces="true" import="java.sql.*,java.util.Date,java.text.DateFormat,java.text.SimpleDateFormat" %>
<%
try {
  Object sessionUser = request.getSession(true).getAttribute("username");
  if (sessionUser == null || sessionUser.toString().isEmpty()) {
    response.sendError(403, "Unauthorized access" );
    return;
  }
} catch (Exception e) {
  response.sendError(403, "Unauthorized access" );
  return;
}

String startDate = request.getParameter("startDate");
String endDate = request.getParameter("endDate");

if (startDate == null || startDate.isEmpty() || endDate == null || endDate.isEmpty() ) {
%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.min.css"/>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script> 
<script type="text/javascript" src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script> 
<script type="text/javascript" src="jquery.validate.js"></script>
<script type="text/javascript" src="jquery.ui.datepicker.validation.js"></script>
        <script>
            $(function () {
                $("#startDate").datepicker({dateFormat: 'dd/mm/yy'});
                $("#endDate").datepicker({dateFormat: 'dd/mm/yy'});

            });
	</script>

	<script>
	$("#frmdownload").validate({ 
    	rules: { 
        	startDate: { 
            		required: true, 
            		dpDate: true 
        		},
		endDate:{
			required:true,
			dpDate: true
    			}
		}     
	});
	</script>
<style>
   .download-form{
     position: fixed;
     top: 50%;
     left: 50%;
     transform: translate(-50%, -50%);

    }

    .btn-grp{
       display: block;
       margin-top: 20px;
       text-align:left;
    }
    .home-btn{
      width:120px;
    }

</style>

  <title>Data Download Statistics</title>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
  <div class="container-fluid download-form">
    <a href="." class="btn btn-primary home-btn">Back to Home</a>
    <h1>Data Download Statistics</h1>
    <div class="row">
      <div class="col-sm-12">
        <p>Please enter start date and end date below</p>
        <form id="frmdownload" class="form-inline" method="get">
          <div class="form-group">
            <label class="sr-only" for="startDate">Start Date</label>
            <input type="text" class="form-control dpDate" id="startDate" name="startDate" placeholder="Start Date">
          </div>
          <div class="form-group">
            <label class="sr-only" for="endDate">End Date</label>
            <input type="text" class="form-control dpDate" id="endDate" name="endDate" placeholder="End Date">
          </div>
          <div class="btn-grp">          
          <button id="btn-submit" type="submit" class="btn btn-default">Download</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</body>
</html>
<% 
} else {

  DateFormat srcDf=new SimpleDateFormat("dd/MM/yyyy");
  Date sdate=srcDf.parse(startDate);
  Date edate=srcDf.parse(endDate);
  DateFormat destDf=new SimpleDateFormat("yyyy-MM-dd");
  startDate=destDf.format(sdate);
  endDate=destDf.format(edate);
 
  if (startDate.indexOf(":") < 0) startDate += " 00:00:00";
  if (endDate.indexOf(":") < 0) endDate += " 23:59:59";
  String driver = "org.postgresql.Driver";
  String url = "jdbc:postgresql://localhost:5432/metacat";
  String username = "metacat";
  String password = "metacat";
  String query = "SELECT to_char(creation_time, 'YYYY-MM-DD HH24:MI:SS') as timestamp, packageid, title, docname, organisation, user_type, reason " +
                 "FROM hirs_download " +
                 "JOIN xml_documents ON hirs_download.docid = xml_documents.docid " + 
                 "JOIN hirs_users ON hirs_download.userid = hirs_users.id " + 
                 "JOIN (SELECT docid, nodedata as title FROM xml_path_index " + 
                       "WHERE path LIKE 'dataset/title') AS a " +
                       "ON a.docid = hirs_download.packageid " +
                 "WHERE creation_time >= ? AND creation_time <= ?";
  Class.forName(driver).newInstance();
  Connection conn = DriverManager.getConnection(url, username, password);
  PreparedStatement ps = conn.prepareStatement(query);
  ps.setTimestamp(1, Timestamp.valueOf(startDate));
  ps.setTimestamp(2, Timestamp.valueOf(endDate));
  ResultSet rs = ps.executeQuery();
  response.setHeader("Content-Disposition", "attachment; filename=usage.csv");
%>timestamp, id, title, filename, organisation, user_type, reason
<%
  while (rs.next()) {
%>
<%=rs.getString("timestamp")%>,<%=rs.getString("packageid")%>,<%=rs.getString("title")%>,<%=rs.getString("docname")%>,<%=rs.getString("organisation")%>,<%=rs.getString("user_type")%>,<%=rs.getString("reason") + "\n"%>
<%
  }
}
%>
