# Introduction #

### What is this repository for? ###
The repository is hosting a source code of the metacat instance deployed at https://live-learning-library.science.uq.edu.au 
as a repository to capture data collected from Heron island research station.


### How do I get set up? ###
The repository can be setup using an ansible script hosted in https://bitbucket.org/ternhq/hirsllp-ansible repository. The source code is customised to the needs of Heron island research station.


### Who do I talk to? ###
The repo is hosted by TERN and accessible by Leslie Elliott from UQ ITS.
Key Contact : Yi Sun (y.sun5@uq.edu.au)  and leslie Elliott (lje@uq.edu.au)
			